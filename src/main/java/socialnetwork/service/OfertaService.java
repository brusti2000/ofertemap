package socialnetwork.service;

import socialnetwork.domain.Oferta;
import socialnetwork.repository.Repository;
import socialnetwork.utils.events.ChangeEventType;
import socialnetwork.utils.events.SchimbareEvent;
import socialnetwork.utils.observer.Observable;
import socialnetwork.utils.observer.Observer;

import java.util.ArrayList;
import java.util.List;

public class OfertaService  implements Observable<SchimbareEvent> {
    private final Repository<Long, Oferta> repoOferta;
    private List<Observer<SchimbareEvent>> observers = new ArrayList<>();

    public OfertaService(Repository<Long, Oferta> repoOferta) {
        this.repoOferta = repoOferta;
    }


    public Oferta addOferta(Oferta Oferta) {
        Oferta user = repoOferta.save(Oferta);

            notifyObserver(new SchimbareEvent());

        //facem validarea in functie de ce ne returneaza .save
        return user;
    }

    /**
     * method that removes the user with id given as a parameter and
     * its friendships
     * @param id - Long, the unique identifier of the user to be deleted
     * @return user that has been removed
     */
    public Oferta deleteOferta(Long id) {
        //3;4
        //3;5
        //6;3
        //sterge in toate prieteniile care il contin pe 3
        Oferta oferta = repoOferta.delete(id);
        //   validatorOfertaService.validateDelete(Oferta);
        if(oferta!=null) {
            notifyObserver(new SchimbareEvent());
        }
        return oferta;
    }

    /**
     * @return all the users
     */
    public Iterable<Oferta> getAll(){
        return repoOferta.findAll();
    }

    public Oferta findOne(Long ID){
        return repoOferta.findOne(ID);
    }

    @Override
    public void addObserver(Observer<SchimbareEvent> e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer<SchimbareEvent> e) {
        observers.remove(e);
    }

    @Override
    public void notifyObserver(SchimbareEvent t) {
        observers.stream().forEach(obs->obs.update(t));
    }
    
}

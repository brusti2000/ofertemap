package socialnetwork.service;


import socialnetwork.domain.Rezervare;
import socialnetwork.repository.Repository;
import socialnetwork.utils.events.SchimbareEvent;
import socialnetwork.utils.observer.Observable;
import socialnetwork.utils.observer.Observer;

import java.util.ArrayList;
import java.util.List;

public class RezervareService  implements Observable<SchimbareEvent> {

    private final Repository<Long, Rezervare> repoRezervare;
    private List<Observer<SchimbareEvent>> observers = new ArrayList<>();

    public RezervareService(Repository<Long, Rezervare> repoRezervare) {
        this.repoRezervare = repoRezervare;
    }


    public Rezervare addRezervare(Rezervare Rezervare) {
        Rezervare user = repoRezervare.save(Rezervare);
        //facem validarea in functie de ce ne returneaza .save
        return user;
    }

    /**
     * method that removes the user with id given as a parameter and
     * its friendships
     * @param id - Long, the unique identifier of the user to be deleted
     * @return user that has been removed
     */
    public Rezervare deleteRezervare(Long id) {
        //3;4
        //3;5
        //6;3
        //sterge in toate prieteniile care il contin pe 3
        Rezervare Rezervare = repoRezervare.delete(id);
        //   validatorRezervareService.validateDelete(Rezervare);
        if(Rezervare!=null) {
            notifyObserver(new SchimbareEvent());
        }
        return Rezervare;
    }

    /**
     * @return all the users
     */
    public Iterable<Rezervare> getAll(){
        return repoRezervare.findAll();
    }

    public Rezervare findOne(Long ID){
        return repoRezervare.findOne(ID);
    }

    @Override
    public void addObserver(Observer<SchimbareEvent> e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer<SchimbareEvent> e) {
        observers.remove(e);
    }

    @Override
    public void notifyObserver(SchimbareEvent t) {
        observers.stream().forEach(obs->obs.update(t));
    }

}

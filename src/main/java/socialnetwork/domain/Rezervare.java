package socialnetwork.domain;

import java.util.Objects;

public class Rezervare extends Entity<Long>{
    private static long idMaxR=0; //pt ca o sa salvez rezervare in fisier
    private String destinatie;
    private String hotel;
    private String perioada;
    private float pret;
    private int nrLocuriDorite;
    private String numeClient;
    private String adresa;

    public Rezervare(String destinatie, String hotel, String perioada, float pret, int nrLocuriDorite, String numeClient, String adresa) {
        this.destinatie = destinatie;
        this.hotel = hotel;
        this.perioada = perioada;
        this.pret = pret;
        this.nrLocuriDorite = nrLocuriDorite;
        this.numeClient = numeClient;
        this.adresa = adresa;
        idMaxR++;
        setId(idMaxR);
    }

    public String getDestinatie() {
        return destinatie;
    }

    public void setDestinatie(String destinatie) {
        this.destinatie = destinatie;
    }

    public String getHotel() {
        return hotel;
    }

    public void setHotel(String hotel) {
        this.hotel = hotel;
    }

    public String getPerioada() {
        return perioada;
    }

    public void setPerioada(String perioada) {
        this.perioada = perioada;
    }

    public float getPret() {
        return pret;
    }

    public void setPret(float pret) {
        this.pret = pret;
    }

    public int getNrLocuriDorite() {
        return nrLocuriDorite;
    }

    public void setNrLocuriDorite(int nrLocuriDorite) {
        this.nrLocuriDorite = nrLocuriDorite;
    }

    public String getNumeClient() {
        return numeClient;
    }

    public void setNumeClient(String numeClient) {
        this.numeClient = numeClient;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rezervare rezervare = (Rezervare) o;
        return Float.compare(rezervare.pret, pret) == 0 &&
                nrLocuriDorite == rezervare.nrLocuriDorite &&
                Objects.equals(destinatie, rezervare.destinatie) &&
                Objects.equals(hotel, rezervare.hotel) &&
                Objects.equals(perioada, rezervare.perioada) &&
                Objects.equals(numeClient, rezervare.numeClient) &&
                Objects.equals(adresa, rezervare.adresa);
    }

    @Override
    public int hashCode() {
        return Objects.hash(destinatie, hotel, perioada, pret, nrLocuriDorite, numeClient, adresa);
    }

    @Override
    public String toString() {
        return "Rezervare{" +
                "destinatie='" + destinatie + '\'' +
                ", hotel='" + hotel + '\'' +
                ", perioada='" + perioada + '\'' +
                ", pret=" + pret +
                ", nrLocuriDorite=" + nrLocuriDorite +
                ", numeClient='" + numeClient + '\'' +
                ", adresa='" + adresa + '\'' +
                '}';
    }
}

package socialnetwork.domain;

import java.util.Objects;

public class Oferta extends Entity<Long>{
    private String destinatie;
    private String hotel;
    private String perioada;
    private float pret;
    private int nrLocuri;

    public Oferta(String destinatie, String hotel, String perioada, float pret, int nrLocuri) {
        this.destinatie = destinatie;
        this.hotel = hotel;
        this.perioada = perioada;
        this.pret = pret;
        this.nrLocuri = nrLocuri;
    }

    public String getDestinatie() {
        return destinatie;
    }

    public void setDestinatie(String destinatie) {
        this.destinatie = destinatie;
    }

    public String getHotel() {
        return hotel;
    }

    public void setHotel(String hotel) {
        this.hotel = hotel;
    }

    public String getPerioada() {
        return perioada;
    }

    public void setPerioada(String perioada) {
        this.perioada = perioada;
    }

    public float getPret() {
        return pret;
    }

    public void setPret(float pret) {
        this.pret = pret;
    }

    public int getNrLocuri() {
        return nrLocuri;
    }

    public void setNrLocuri(int nrLocuri) {
        this.nrLocuri = nrLocuri;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Oferta oferta = (Oferta) o;
        return Float.compare(oferta.pret, pret) == 0 &&
                nrLocuri == oferta.nrLocuri &&
                Objects.equals(destinatie, oferta.destinatie) &&
                Objects.equals(hotel, oferta.hotel) &&
                Objects.equals(perioada, oferta.perioada);
    }

    @Override
    public int hashCode() {
        return Objects.hash(destinatie, hotel, perioada, pret, nrLocuri);
    }

    @Override
    public String toString() {
        return "Oferta{" +
                "destinatie='" + destinatie + '\'' +
                ", hotel='" + hotel + '\'' +
                ", perioada='" + perioada + '\'' +
                ", pret=" + pret +
                ", nrLocuri=" + nrLocuri +
                '}';
    }
}

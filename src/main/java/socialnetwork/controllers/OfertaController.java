package socialnetwork.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import socialnetwork.domain.Oferta;
import socialnetwork.domain.Rezervare;
import socialnetwork.service.OfertaService;
import socialnetwork.service.RezervareService;
import socialnetwork.utils.events.SchimbareEvent;
import socialnetwork.utils.observer.Observer;

import java.util.ArrayList;
import java.util.List;


public class OfertaController  implements Observer<SchimbareEvent> {
    ObservableList<Oferta> modelOferte= FXCollections.observableArrayList();
    ObservableList<Oferta> modelOferteFiltrate= FXCollections.observableArrayList();
    OfertaService ofertaService;
    RezervareService rezervareService;

    Stage ofertaStage;
    @FXML
    TableView<Oferta> idTableOferte;
    @FXML
    TableColumn<Oferta,String>idDestinatie;
    @FXML
    TableColumn<Oferta,String>idHotel;
    @FXML
    TableColumn<Oferta,String>idPerioada;
    @FXML
    TableColumn<Oferta, Float>idPret;
    @FXML
    TableColumn<Oferta,Integer>idLocuriDisp;
    @FXML
    TableView<Oferta> idTableOferteF;
    @FXML
    TableColumn<Oferta,String>idDestinatieF;
    @FXML
    TableColumn<Oferta,String>idHotelF;
    @FXML
    TableColumn<Oferta,String>idPerioadaF;
    @FXML
    TableColumn<Oferta, Float>idPretF;
    @FXML
    TableColumn<Oferta,Integer>idLocuriDispF;
    @FXML
    Button idButton;
    @FXML
    TextField idTextField;
    @FXML
    TextField idNume;
    @FXML
    TextField idAdresa;
    @FXML
    TextField idNrLocuri;
    @FXML
    Button idButtonRezerva;



    @FXML
    public void initialize(){
        idDestinatie.setCellValueFactory(new PropertyValueFactory<Oferta,String>("destinatie"));
        idHotel.setCellValueFactory(new PropertyValueFactory<Oferta,String>("hotel"));
        idPerioada.setCellValueFactory(new PropertyValueFactory<Oferta,String>("perioada"));
        idPret.setCellValueFactory(new PropertyValueFactory<Oferta,Float>("pret"));
        idLocuriDisp.setCellValueFactory(new PropertyValueFactory<Oferta,Integer>("nrLocuri"));

        idTableOferte.setItems(modelOferte);

        idDestinatieF.setCellValueFactory(new PropertyValueFactory<Oferta,String>("destinatie"));
        idHotelF.setCellValueFactory(new PropertyValueFactory<Oferta,String>("hotel"));
        idPerioadaF.setCellValueFactory(new PropertyValueFactory<Oferta,String>("perioada"));
        idPretF.setCellValueFactory(new PropertyValueFactory<Oferta,Float>("pret"));
        idLocuriDispF.setCellValueFactory(new PropertyValueFactory<Oferta,Integer>("nrLocuri"));

        idTableOferteF.setItems(modelOferteFiltrate);

    }


    private void initModel() {
        // datele care se incarca in tabel
        Iterable<Oferta>oferte=ofertaService.getAll();

        List<Oferta> oferteFiltrate=new ArrayList<>();

        oferte.forEach(o->{
            if(o.getNrLocuri()>0)
            oferteFiltrate.add(o);
       //     System.out.println(o);}
        });
        modelOferte.setAll(oferteFiltrate);

    }


    public void setAttributes(OfertaService ofertaService, RezervareService rezervareService, Stage ofertaStage) {
        this.ofertaService=ofertaService;
        this.rezervareService=rezervareService;
        this.ofertaStage=ofertaStage;
        ofertaService.addObserver(this);
        rezervareService.addObserver(this);
        initModel();
    }

    @Override
    public void update(SchimbareEvent stareChangeEvent) {
        initModel();
    }


    public void filtrareOferte() {
        String text=idTextField.getText();
        Iterable<Oferta> oferteAll=ofertaService.getAll();
        List<Oferta>oferteFiltrate=new ArrayList<>();
        oferteAll.forEach(o->{
            if( o.getDestinatie().contains(text))
                oferteFiltrate.add(o);
        });
        modelOferteFiltrate.setAll(oferteFiltrate); //init model pentru celalalt tabel
    }

    public void clearFiltrare(){
        List<Oferta>list=new ArrayList<>();

        modelOferteFiltrate.setAll(list);
        idTextField.clear();
    }
    public void rezervaOferta() {
        Oferta selectedOferta=idTableOferteF.getSelectionModel().getSelectedItem();
        if(selectedOferta!=null)
        {       String numeClient=idNume.getText();
                String adresa=idAdresa.getText();
                String nrLocuri=idNrLocuri.getText();
                int nr= selectedOferta.getNrLocuri()-Integer.parseInt(nrLocuri);


                if(nr>=0) {
                    Rezervare rezervare=new Rezervare(selectedOferta.getDestinatie(),selectedOferta.getHotel(),selectedOferta.getPerioada(),
                            selectedOferta.getPret(),Integer.parseInt(nrLocuri),numeClient,adresa);
                    rezervareService.addRezervare(rezervare);
                    Alert alert= new Alert(Alert.AlertType.INFORMATION,"S-a rezervat cu succes!");
                    alert.show();
                    idNume.clear();
                    idAdresa.clear();
                    idNrLocuri.clear();
                    ofertaService.deleteOferta(selectedOferta.getId());

                    if(nr>0){

                    Oferta oferta=new Oferta(selectedOferta.getDestinatie(),selectedOferta.getHotel(),selectedOferta.getPerioada()
                            ,selectedOferta.getPret(),nr);

                    oferta.setId(selectedOferta.getId());

                    ofertaService.addOferta(oferta);
                          }
                    clearFiltrare();
                    initModel();

                        }
                else
                {
                    Alert alert= new Alert(Alert.AlertType.ERROR,"Locuri indisponibile!");
                    alert.show();
                }

        }else{
            Alert alert= new Alert(Alert.AlertType.ERROR,"Nu ati selectat nimic");
            alert.show();
        }
    }
}

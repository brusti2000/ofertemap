package socialnetwork.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import socialnetwork.domain.Oferta;

import socialnetwork.service.OfertaService;
import socialnetwork.service.RezervareService;
import socialnetwork.utils.events.SchimbareEvent;

import socialnetwork.utils.observer.Observer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class IntroducereController implements Observer<SchimbareEvent> {

  OfertaService ofertaService;
  RezervareService rezervareService;
  List<String>argumente;


    ObservableList<Oferta> modelOferta= FXCollections.observableArrayList();




    public void setIntroductionStage(Stage introductionStage) {



        argumente.forEach(a->{
            showOfertaStage(a);
        });

    }

   @FXML
    public void initialize(){
     /*   tableColumnNume.setCellValueFactory(new PropertyValueFactory<Membru,String>("Name"));
        tableColumnRol.setCellValueFactory(new PropertyValueFactory<Membru,String>("Rol"));
        tableColumnStare.setCellValueFactory(new PropertyValueFactory<Membru,String>("Stare"));
        tableViewMembrii.setItems(modelMembru);*/

    }


    private void initModel(){
       Iterable<Oferta>oferte=this.ofertaService.getAll();

        List<Oferta> oferteFiltrate=new ArrayList<>();
        oferte.forEach(o->{
            if(o.getNrLocuri()>0)
                oferteFiltrate.add(o);
        });
        modelOferta.setAll(oferteFiltrate);



    }
    public void setOfertaService(OfertaService ofertaService, Stage primaryStage,List<String>argumente) {
        this.ofertaService=ofertaService;
        this.ofertaService.addObserver(this);
        this.argumente=argumente;
        initModel();

    }


    public void setRezervareService(RezervareService rezervareService) {
        this.rezervareService=rezervareService;
        this.rezervareService.addObserver(this);


    }





    private void showOfertaStage(String name){
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/views/oferta.fxml"));
            AnchorPane root = loader.load();

            Stage ofertaStage = new Stage();
            ofertaStage.setTitle(name+"'s account");

            Scene scene = new Scene(root);
            ofertaStage.setScene(scene);
            OfertaController ofertaController=loader.getController();

            ofertaController.setAttributes(ofertaService,rezervareService,ofertaStage);


      //    introductionStage.hide();
         ofertaStage.show();

        }catch(IOException e){
            e.printStackTrace();
        }
    }





    @Override
    public void update(SchimbareEvent stareChangeEvent) {
        modelOferta.setAll((Collection<? extends Oferta>) this.ofertaService.getAll());
    }
}

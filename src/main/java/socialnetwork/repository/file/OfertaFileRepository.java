package socialnetwork.repository.file;

import socialnetwork.domain.Oferta;


import java.util.List;

public class OfertaFileRepository extends AbstractFileRepository<Long, Oferta>{

    public OfertaFileRepository( String fileName) {
        super( fileName);
    }

    @Override
    public Oferta extractEntity(List<String> attributes) {
        //TODO: implement method
        Oferta oferta = new Oferta(attributes.get(1),attributes.get(2),attributes.get(3),Float.parseFloat(attributes.get(4))
                , Integer.parseInt(attributes.get(5)));
        oferta.setId(Long.parseLong(attributes.get(0)));

        return oferta;
    }

    @Override
    protected String createEntityAsString(Oferta entity) {
        return entity.getId()+";"+entity.getDestinatie()+";"+entity.getHotel()+";"+entity.getPerioada()+";"+entity.getPret()+";"
                + entity.getNrLocuri();
    }


}

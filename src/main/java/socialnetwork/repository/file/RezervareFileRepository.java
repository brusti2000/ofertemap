package socialnetwork.repository.file;

import socialnetwork.domain.Oferta;
import socialnetwork.domain.Rezervare;

import java.util.List;

public class RezervareFileRepository extends AbstractFileRepository<Long, Rezervare>{

    public RezervareFileRepository(String fileName) {
        super(fileName);
    }

    @Override
    public Rezervare extractEntity(List<String> attributes) {
        Rezervare rezervare = new Rezervare(attributes.get(1),attributes.get(2),attributes.get(3),Float.parseFloat(attributes.get(4))
                , Integer.parseInt(attributes.get(5)), attributes.get(6),attributes.get(7));
        rezervare.setId(Long.parseLong(attributes.get(0)));

        return rezervare;

    }

    @Override
    protected String createEntityAsString(Rezervare entity) {
        return entity.getId()+";"+entity.getDestinatie()+";"+entity.getHotel()+";"+entity.getPerioada()+";"+entity.getPret()+";"
                + entity.getNrLocuriDorite()+";"+entity.getNumeClient()+";"+entity.getAdresa();
    }
}

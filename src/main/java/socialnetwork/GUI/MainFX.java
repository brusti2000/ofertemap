package socialnetwork.GUI;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import socialnetwork.config.ApplicationContext;
import socialnetwork.controllers.IntroducereController;
import socialnetwork.domain.Oferta;
import socialnetwork.domain.Rezervare;
import socialnetwork.repository.Repository;
import socialnetwork.repository.file.OfertaFileRepository;
import socialnetwork.repository.file.RezervareFileRepository;
import socialnetwork.service.OfertaService;
import socialnetwork.service.RezervareService;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainFX extends Application {


    private static RezervareService rezervareService;
    private static OfertaService ofertaService;
    private static List<String> argumente=new ArrayList<>();


    @Override
    public void start(Stage primaryStage) throws Exception {
        initView(primaryStage);

        //     primaryStage.show();
    }

    public static void main(String[] args){
        String fileNameOferte=ApplicationContext.getPROPERTIES().getProperty("data.socialnetwork.oferte");
        String fileNameRezervari=ApplicationContext.getPROPERTIES().getProperty("data.socialnetwork.rezervari");


        Repository<Long, Oferta> oferteFileRepository = new OfertaFileRepository(fileNameOferte);
        Repository<Long, Rezervare> rezervareFileRepository = new RezervareFileRepository(fileNameRezervari);



     rezervareService=new RezervareService(rezervareFileRepository);
         ofertaService=new OfertaService(oferteFileRepository);

        for (int i=0;i<args.length;i++)
            argumente.add(args[i]);

     //  argumente.forEach(System.out::println);

        launch(args);
    }
    private void initView(Stage primaryStage) throws IOException {


        FXMLLoader loader=new FXMLLoader();
        loader.setLocation(getClass().getResource("/views/introduction.fxml"));

        AnchorPane layout = loader.load();
        primaryStage.setScene(new Scene(layout));
        IntroducereController introducereController =loader.getController();

        introducereController.setOfertaService(ofertaService,primaryStage,argumente);
        introducereController.setRezervareService(rezervareService);
        introducereController.setIntroductionStage(primaryStage);




    }
}

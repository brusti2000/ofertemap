package socialnetwork.utils.events;



public class SchimbareEvent implements Event{


    private ChangeEventType eventType;

    public SchimbareEvent() {

    }

    public ChangeEventType getEventType() {
        return eventType;
    }

    public void setEventType(ChangeEventType eventType) {
        this.eventType = eventType;
    }


}
